<?php

namespace Raddit\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="message_replies")
 */
class MessageReply extends Message {
    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(targetEntity="MessageThread", inversedBy="replies")
     *
     * @var MessageThread
     */
    private $thread;

    /**
     * @ORM\OneToMany(targetEntity="MessageReplyNotification", mappedBy="reply", cascade={"persist", "remove"})
     *
     * @var Notification[]|Collection|Selectable
     */
    private $notifications;

    public function __construct(User $sender, string $body, $ip, MessageThread $thread) {
        parent::__construct($sender, $body, $ip);

        $this->thread = $thread;
        $this->notifications = new ArrayCollection();
    }

    public function getThread(): MessageThread {
        return $this->thread;
    }

    /**
     * @return Notification[]|Collection|Selectable
     */
    public function getNotifications() {
        return $this->notifications;
    }
}
